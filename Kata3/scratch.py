# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 18:42:11 2018

@author: User
"""

from animal import mammal
print(mammal.dog.speak())
print(mammal.human.speak())

from animal import bird
print(bird.eagle.speak())
print(bird.sparrow.speak())
